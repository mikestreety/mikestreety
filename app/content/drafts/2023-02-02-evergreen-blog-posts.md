---
title: Writing more evergreen blog post topics
date: 2023-02-02
intro:
# permalink: "blog/evergreen-blog-posts/"
tags:
  - General
  - Ramblings
---

Evergreen topics are the golden egg of blog posting; posts which don't age and don't become irrelevant over time. I really wish I wrote more blog posts which weren't about a specific framework or version number. A lot of my posts are about the here and now "this is how you do this thing with this technology". They might still be right in 1, 2 or 3 years time, or they might not.

Even my most popular posts about [Migrating Gitlab to another server](/blog/migrating-gitlab-from-one-server-to-another/ "Gitlab") or [Creating a Docusaurus Docker container](/blog/creating-a-docusaurus-docker-image-for-consistent-rendering-of-your-documentation/) website can easily be rendered useless with the release of a new version or a change in the commands to be run.

It's not easy creating evergreen content - I envy those that can come up with philosophical or timeless talks, too. If you want to be seen as a "thought leader" (horrible phrase, but you get my drift) then you need to start creating blog posts and presentations that don't age and will always be relevant.

I have managed to write *some* evergreen blog posts - my [Git commit hash](/blog/the-git-commit-hash/) and "[Making sure you're not doing everything at work](/blog/making-sure-youre-not-doing-everything-at-work/)" posts don't have an expiry date (unless Git fundamentally change the way hashes are made).
